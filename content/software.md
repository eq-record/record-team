---
#title: "software.md"
date: 2025-01-13T16:33:47+01:00
draft: false
---




* __[SIWAA](https://siwaa.toulouse.inrae.fr/)__: the SIWAA 
(Workbench for Agroecosystems SImulations and Analytics) website is based on a 
[Galaxy](https://galaxyproject.org/) solution to provide access to simulation tools,
calibration of models. Available tools are containerized to stabilize solutions
and provide more sustainability. The cluster [MESO@LR](https://meso-lr.umontpellier.fr/) 
is mobilized for access to computing resources.

* __[VLE](https://www.vle-project.org/)__ : the RECORD team co-develops with the 
[SCIDYN](https://miat.inrae.fr/site/SCIDYN(english)) research team (INRAE - MIAT)
the VLE (Virtual Laboratory Environment) software for simulating discrete event systems 
based on the [DEVS](https://en.wikipedia.org/wiki/DEVS) (Discrete Event System Specification) formalism. 
This C++ software is mobilized by the RECORD team to implement models of agroecosystems.

* __[erecord](http://erecord.toulouse.inra.fr/)__: relying on a 
[REST API](https://en.wikipedia.org/wiki/Representational_state_transfer#Applied_to_web_services),
RECORD team provides the web service erecord, that allows the remote simuation of 
models available on the VLE software.
