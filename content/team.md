---
#title: "Members & Contact"
date: 2025-01-13T11:17:34+01:00
draft: false
tableClass: transparentTable
---

To contact all staff members at once : [recordNum(at)groupes.renater.fr](mailto:record_num@groupes.renater.fr)

 | | | 
-|-|-|-
{{< figure src="img/trombinoscope/Estelle.png" alt="Estelle" height="100" >}} | Estelle Ancelet (40% FTE)| IE INRAE | [estelle.ancelet(at)inrae.fr](mailto:estelle.ancelet@inrae.fr)
{{< figure src="img/trombinoscope/Eric.png" alt="Eric" height="100">}} | Eric Casellas (90% FTE)| IE INRAE | [eric.casellas(at)inrae.fr](mailto:eric.caselas@inrae.fr)
{{< figure src="img/trombinoscope/Patrick.png" alt="Patrick" height="100" >}} | Patrick Chabrier (60% FTE)| IR INRAE | [patrick.chabrier(at)inrae.fr](mailto:patrick.chabrier@inrae.fr)
{{< figure src="img/trombinoscope/Nathalie.png" alt="Nathalie" height="100" >}}  | Nathalie Rousse (100% FTE)| IR INRAE | [nathalie.rousse(at)inrae.fr](mailto:nathalie.rousse@inrae.fr)
{{< figure src="img/trombinoscope/Ronan.png" alt="Ronan" height="100" >}} | Ronan Trépos (100% FTE) - Team leader | IR INRAE | [ronan.trepos(at)inrae.fr](mailto:ronan.trepos@inrae.fr)  


