---
#title: "Publications"
date: 2025-01-13
draft: false
bib: RECORD
---

* __To acknowledge the platform__  

    - To acknowledge the platform into your aticles, please cite this:
    >"This work has been supported by the platform RECORD from MIAT-INRAE".  
    
    - You can also cite the platform using the following reference:
    >J.-E. Bergez, P. Chabrier, C. Gary, M.H. Jeuffroy, D. Makowski, G. Quesnel, E. Ramat, H. Raynal, N. Rousse, D. Wallach, P. Debaeke, P. Durand, M. Duru, J. Dury, P. Faverdin, C. Gascuel-Odoux, F. Garcia, An open platform to build, evaluate and simulate integrated models of farming and agro-ecosystems, Environmental Modelling & Software, Volume 39, 2013, Pages 39-49, https://doi.org/10.1016/j.envsoft.2012.03.011.

***

* __Recent articles__


{{< bibliography >}}

