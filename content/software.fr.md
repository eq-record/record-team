---
#title: "software.fr.md"
date: 2025-01-13T16:33:47+01:00
draft: false
---

* __[SIWAA](https://siwaa.toulouse.inrae.fr/)__: le site web SIWAA (Workbench for Agroecosystems SImulations and Analytics),
repose sur une solution [Galaxy](https://galaxyproject.org/) pour proposer l'accès à des outils de simulation, 
de calibration de modèles, et plus généralement de traitement de 
données pour l'étude des agroecosystèmes. Les outils sont conteneurisés pour stabiliser les solutions 
et offrir plus de pérennité. La grappe de calcul [MESO@LR](https://meso-lr.umontpellier.fr/) est mobilisé pour l'accès au 
ressources de calcul.

* __[VLE](https://www.vle-project.org/)__ : l'equipe co-développe avec l'équipe [SCIDYN](https://miat.inrae.fr/site/SCIDYN) 
(INRAE - MIAT) le logiciel VLE (_Virtual Laboratory Environment_) pour la modélisation et la simulation à 
évènements discrets reposant sur le formalisme [DEVS](https://fr.wikipedia.org/wiki/Discrete_Event_System_Specification) 
(_Discrete Event System Specification_). Cette solution, codée en C++, est mobilisée par l'équipe pour mettre en oeuvre 
des modèles d'agroécosystèmes.

* __[erecord](http://erecord.toulouse.inra.fr/)__: l'équipe propose le service web erecord qui est une 
[API REST](https://fr.wikipedia.org/wiki/Representational_state_transfer#Appliqu%C3%A9_aux_services_web)  permettant 
d'éxécuter à distance des simulateurs disponibles dans VLE.


