---
#title: "projects.fr.md"
date: 2025-01-13T16:33:47+01:00
draft: false
---

---
* __ANR OPTILEG - Appel à projets PROT-LEG (2024-2029)__
	- description: le projet OPTILEG (Optimisation des interactions microbiennes pour une production 
	durable de protéines de légumineuses, bénéfique pour l’environnement et la santé) étudie l'optimisation
	des interactions des légumineuses avec les microorganismes du sol pour promouvoir la production 
	à bas intrants de protéines.
	- collaboration: [UMR Agroécologie](https://umr-agroecologie.dijon.hub.inrae.fr/) 
	- activités de l'équipe: modélisation, simulation et calibration
	- membres de l'équipe impliquées: @Ronan.Trépos, @Eric.Casellas
---
* __ANR INSERER LES - Appel à projets PROT-LEG (2024-2027)__
	- description: le projet INSERER LES (INSERtion Reussie des LEgumineuses à graines dans les Systèmes 
	alimentaires et de culture) étudie les stratégies d'action à mener pour augmenter la production des 
	légumineuses à graines dans les systèmes de culture ainsi que leur place dans 
	la transformation et la consommation en France
	- collaboration: [UMR Agroécologie](https://umr-agroecologie.dijon.hub.inrae.fr/) 
	- activités de l'équipe: modélisation, simulation et calibration
	- membres de l'équipe impliquées: @Ronan.Trépos, @Eric.Casellas
-----
* __Projet emblématique INSILICOW du métaprogramme [DIGIT-BIO](https://digitbio.hub.inrae.fr/metaprogramme-digit-bio) (2024-2027)__
	- description: le projet INSILICOW (Du modèle au jumeau numérique d'une exploitation de vaches laitières) a pour objectif 
	de rendre opérationnel le concept de jumeau numérique d'une ferme laitière en un outil innovant, et d'établir la preuve 
	de concept de son utilité pour son étude et sa gestion.
	- collaboration: [UMR MoSAR](https://mosar.versailles-grignon.hub.inrae.fr/umr-mosar) 
	- activités de l'équipe: expertise ingnieurie informatique et modélisation
	- membres de l'équipe impliquées: @Ronan Trépos, @Patrick.Chabrier
-----
* __ANR CHIP-GT (2023-2026)__
	- description: le projet CHIP-GT (Coordination d’agents de planification 
	hétérogènes en interaction à l'aide de la théorie des jeux) développe des 
	méthodes pour la planification automatique d'agents hétérogène en se reposant sur la théore des jeux. Des applications en simulation 
	pour la conservation d'espèces sont traitées au sein du projet
	- collaboration: [UR MIAT-Scidyn](https://miat.inrae.fr/teams/scidyn/) 
	- activités de l'équipe: accompagnement de l'ingénierie logicielle : automatisation, bonnes pratiques, déploiement, utilisation de forgeMIA
	- membres de l'équipe impliquées: @Eric.Casellas, @Patrick.Chabrier
-----	
* __Projet Institut Carnot Plant2Pro PIFEC (2023-2025)__
	- description: le projet PIFEC (Vers une méthode de Pilotage Intégral de la FErtilisation azotée du Colza d’hiver) 
	développe une méthode pour le pilotage intégral de la fertilisation azotée pour le colza d'hiver. Cet outil reposer 
	entre autres sur la modélisation et la simulation.
	- collaboration: [UMR Agronomie](https://www6.versailles-grignon.inrae.fr/agronomie/Recherche), [Terres Inovia](https://www.terresinovia.fr/)
	- activités de l'équipe: modélisation, simulation et assimilation de données
	- membres de l'équipe impliquée: @Ronan.Trépos, @Eric.Casellas
-----
* __ANR [HSMM-INCA](https://anr.fr/Project-ANR-21-CE40-0005) (2022-2025)__ 
	- description: le projet HSMM-INCA (_Hidden Semi Markov Models_: INference, Control and Applications) développe des méthodes 
	  et algorithmes pour l'inférence de modèles HSMM. Plusieurs applications, dont l'etude de la migration d'oiseaux, 
	  sont également traitées dans ce projet. 
	- collaboration: [UR MIAT-Scidyn](https://miat.inrae.fr/teams/scidyn/)
	- activités de l'équipe: implémentation de modèles et méthodes, et experimentations numériques.
	- membres de l'équipe impliqués: @Ronan.Trépos
-----
* __European Joint Program Soil - Internal Call [CarboSeq](https://ejpsoil.eu/soil-research/carboseq) (2022-2025)__
	- description: le projet CarboSeq (_Soil organic carbon sequestration potentialof agricultural soils in Europe_) 
	cherche à estimer la séquestration potentielle du carbone organique des sols cultivés européens.
	- collaboration: [US InfoSol](https://info-et-sols.val-de-loire.hub.inrae.fr/)
	- activités de l'équipe: déploiement d'une boîte à outils de simulation (séquestration du Carbone dans le Sol) sur SIWAA.
	- membres de l'équipe impliqués: @Eric.Casellas, @Patrick.Chabrier
-----
* __Projet exploratoire [REDELAC](https://www6.inrae.fr/climae/Thematiques/Conception-et-evaluation-de-systemes-durables/REDELAC) 
du metaprogramme [CLIMAE](https://www6.inrae.fr/climae/) (2023 - 2024)__
	- description: le projet REDELAC (Résilience Et Durabilité des Exploitations Laitières 
	de plaine face aux Aléas Climatiques) ambitionne de simuler l’évolution à moyen-terme 
	du fonctionnement des exploitations laitières de plaine, de regarder l’impact du climat futur 
	et de l’incertitude associée. Ce projet exploratoire interdisciplinaire sera cantonné 
	à un territoire breton.
	- collaboration: [UMR PEGASE](https://pegase.rennes.hub.inrae.fr/), [UMR MoSAR](https://www6.jouy.inrae.fr/mosar/UMR-MoSAR)
	- activités de l'équipe: traitement de données, déploiement d'applications, développement 
	d'une chaine de simulations, simulations sur ressources de calcul de type HPC
	- membres de l'équipe impliqués: @Eric.Casellas, @Patrick.Chabrier
-----	



	 
