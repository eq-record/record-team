---
#title: "La page d'acceuil"
date: 2025-01-13T16:33:47+01:00
draft: false
---

Le dispositif collectif RECORD a pour mission de mobiliser des outils informatiques 
et mathématiques dans le cadre de la modélisation et la simulation d'agroécosystèmes 
et de systèmes écologiques. L'objectif est d'accompagner les modélisateurs agronomes
et écologues dans la mise en œuvre de solutions pour l'informatisation et la 
simulation de leurs modèles. Les actions proposées par le dispositif sont les 
suivantes :


{{< detail-tag "1. **_Accompagner au développement et à la maintenance de simulateurs_**" >}}

* Mise en œuvre de langages de programmation tels que C++, R, Python, fortran et 
mobilisation de suites logicielles dédiées à la modélisation et la simulation telles 
que [VLE](https://www.vle-project.org/).

* Amélioration des performances des simulateurs (par exemple à travers l'utilisation 
de [Rcpp](https://www.rcpp.org/) pour les codes R ou 
[pybind11](https://pybind11.readthedocs.io/en/stable/index.html) pour les codes 
python).

{{< /detail-tag >}}


{{< detail-tag "2. **_Déployer des applications autour des simulateurs_**" >}}

* Empaquetage de simulateurs et d'outils logiciels pour la simulation 
(empaquetage [conda](https://docs.conda.io/en/latest/) et R, conteneurisation 
[docker](https://www.docker.com/), 
[Singularity](https://docs.sylabs.io/guides/latest/user-guide/))

* Mise à disposition de simulateurs à travers l'internet selon des approches 
SaaS, sous forme de chaînes de traitement de type 
[Galaxy](https://galaxyproject.org/), de services web (API REST), 
d'applications web (de type [RShiny](https://shiny.rstudio.com/) pour la visualisation...).

{{< /detail-tag >}}


{{< detail-tag "3. **_Mobiliser des ressources de calcul intensif pour la simulation_**" >}}

* Mise en œuvre de chaînes de traitements de type [Galaxy](https://galaxyproject.org/) 
sur des grappes de serveur de calcul (en particulier [MESO@LR](https://meso-lr.umontpellier.fr/))

* Accompagnement à l'utilisation de ressources pour le calcul haute performance 
(utilisation d'un ordonnanceur [Slurm](https://slurm.schedmd.com/overview.html), ...)

{{< /detail-tag >}}


{{< detail-tag "4. **_Accompagner à l'utilisation de méthodes d'exploration de modèles numériques_**" >}}

* Mobilisation de méthodes formelles d'analyse et de calibration de modèles.

{{< /detail-tag >}}


Au quotidien, les agents du dispositif mobilisent des outils facilitant le développement 
collaboratif et la diffusion de leur travail (utilisation intensive de forges) ainsi 
que la qualité et la reproductibilité 
de la production (versionnement, conteneurisation, intégration et livraison continues, 
_infrastructure as code_ ...).

***

Le dispositif collectif RECORD est constitué d'une équipe d'ingénieurs en 
informatique et en calcul scientifique du département [MathNum](https://www.inrae.fr/departements/mathnum). 
Les membres de l'équipe ont également des responsabilités dans divers réseaux et 
projets connexes aux activités du dispositif collectif:
* Membre du groupe projet [STICS](https://stics.inrae.fr/). Le modèle de culture 
STICS est mobilisé dans de nombreux projets dans lesquels l'équipe du dispositif est impliquée.
* Equipe de développement de la forge [forgeMIA](https://forgemia.inra.fr/). 
L'équipe repose sur cette forge pour la plupart de ces projets de gestion de code 
et d'intégration continue.
* Bureau du réseau [Mexico](https://reseau-mexico.fr/). Ce réseau facilite le partage de 
compétences et d'expériences autour des méthodes d'exploration de modèles numériques. 





