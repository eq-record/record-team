---
#title: "The welcome page"
date: 2025-01-13T16:17:34+01:00
draft: false
---
The RECORD platform aims to mobilize IT tools and mathematics in the 
context of modeling and simulating agroecosystems and ecological systems. 
The objective is to support agronomist and ecologists modellers
in the implementation of solutions for computerization and 
simulation of their models. The actions proposed by the platform are the
following:

{{< detail-tag "1. **_Support in the development and maintenance of simulators_**" >}}

* Implementation of programming languages ​​such as C++, R, Python, Fortran and
mobilization of software suites dedicated to modeling and simulation such as
that [VLE](https://www.vle-project.org/).

* Improvment of simulators performances (through for example the use of 
[Rcpp](https://www.rcpp.org/) for R codes or 
[pybind11](https://pybind11.readthedocs.io/en/stable/index.html) for python codes).

{{< /detail-tag >}}

{{< detail-tag "2. **_Deployment of simulator based application_**" >}}

* Packaging simulators and software tools for simulation 
(packages [conda](https://docs.conda.io/en/latest/) and R, containerization with 
[docker](https://www.docker.com/) and
[Singularity](https://docs.sylabs.io/guides/latest/user-guide/))

* Provision of simulators through the Internet according to approaches
SaaS, such as workflows [Galaxy](https://galaxyproject.org/), web services (REST API), 
web applications (for example [RShiny](https://shiny.rstudio.com/) for vizualisation purposes).

{{< /detail-tag >}}

{{< detail-tag "3. **_Mobilization of HPC (high-performance computing) for simulation_**" >}}

* Implement [Galaxy](https://galaxyproject.org/) worflows on clusters such 
as [MESO@LR](https://meso-lr.umontpellier.fr/). 

* Support in the use of HPC ressources such as the [Slurm](https://slurm.schedmd.com/overview.html)
 job scheduling system.

{{< /detail-tag >}}

{{< detail-tag "4. **_Support in the use of formal methods to study numerical models_**" >}}

* Mobilization of formal methods for sensitivy analyses and calibration of numerical models.

{{< /detail-tag >}}

The agents of the RECORD platform mobilize tools facilitating 
the collaborative development and the dissemination of their work 
(intensive use of forges) and focuse on the quality and reproducibility of their 
realization (code versioning, containerization, continuous integration / continuous develoment,
infrastructure as code ...).

***

RECORD is based on a team of engineers of the  [MathNum](https://www.inrae.fr/en/divisions/mathnum) 
department of [INRAE](https://www.inrae.fr/en).
Team members also have responsibilities in various networks and
projects related to the activities of the platform:
* Devlopment team of the [forgeMIA](https://forgemia.inra.fr/) Gitlab forge.
RECORD engineers rely on this forge for most of their projects.
* Committee bureau of the [Mexico](https://reseau-mexico.fr/) network. This network 
facilitates the sharing of skills and experience around methods for exploring numerical models.
* The [STICS](https://stics.inrae.fr/eng) project team. The crop model
STICS is mobilized in many projects of the platform.
