---
#title: "projects.fr.md"
date: 2025-01-13T16:33:47+01:00
draft: false
---

---
* __ANR OPTILEG - Call for proposals PROT-LEG (2024-2029)__
	- description: the project OPTILEG (Optimization of microbial interactions for sustainable production
	of legume proteins, beneficial for the environment and health) aims to optimize 
	the legumes interactions with beneficial soil microorganisms to promote protein production 
	at low input levels
	- collaborators: [UMR Agroécologie](https://eng-umr-agroecologie.dijon.hub.inrae.fr/) 
	- activities of RECORD: modeling, simulation and estimation
	- involved team members: @Ronan.Trépos, @Eric.Casellas
-----
* __ANR INSERER LES - Call for proposals PROT-LEG (2024-2027)__
	- description: the project INSERER LES (Succesfully inserting grain legumes in food and cropping systems) 
	studies the action strategies to be taken to increase the production of grain legumes in cropping systems 
	at low input levels
	- collaborators: [UMR Agroécologie](https://eng-umr-agroecologie.dijon.hub.inrae.fr/) 
	- activities of RECORD: modeling, simulation and estimation
	- involved team members: @Ronan.Trépos, @Eric.Casellas
-----
* __Flagship Project INSILICOW of the [DIGIT-BIO](https://digitbio.hub.inrae.fr/metaprogramme-digit-bio) métaprogramme (2024-2027)__
	- description: the objective of the project INSILICO (From digital model to digital twin of dairy cattle farming system) 
	is to operationalize the concept of a dairy farm digital twin into an innovating user-friendly online tool, and to 
	establish the proof of concept of its usefulness to study and manage dairy systems.
	- collaborators: [UMR MoSAR](https://eng-mosar.versailles-grignon.hub.inrae.fr/umr-mosar) 
	- activities of RECORD: expertise in computer engineering and modeling
	- involved team members: @Ronan Trépos, @Patrick.Chabrier
-----
* __ANR CHIP-GT (2023-2026)__
	- description: The CHIP-GT project (Coordinating Heterogeneous Interacting Planning Agents Using Game Theory) 
	develops methods for the multi-agent planning using game theory. Applications on the simulation of conservation 
	issues will be undergone.
	- collaborators: [UR MIAT-Scidyn](https://miat.inrae.fr/teams/scidyn/) 
	- activities of RECORD: support in software develoment: good practises, deployment, use of forgeMIA
	- involved team members: @Eric.Casellas, @Patrick.Chabrier
-----
* __Projet Institut Carnot Plant2Pro PIFEC (2023-2025)__
	- description: The PIFEC project (Towards an integral management method for nitrogen fertilization in winter rapeseed) 
	develops a new method for steering nitrogen fertilisation of rapeseed. This mehtod will rely on the crop modeling and simulation.
	- collaborators: [UMR Agronomie](https://www6.versailles-grignon.inrae.fr/agronomie_eng/Research), [Terres Inovia](https://www.terresinovia.fr/)
	- activities of RECORD: modeling, simulation and data assimilation
	- involved team members: @Ronan.Trépos, @Eric.Casellas
-----
* __ANR [HSMM-INCA](https://anr.fr/Project-ANR-21-CE40-0005) (2022-2025)__ 
	- description: The HSMM-INCA project (Hidden Semi Markov Models: INference, Control and Applications) develops 
	methods and algorithms for the inference of HSMM models. 
	Several applications, such as the study of birds migration, are studied.
	- collaborators: [UR MIAT-Scidyn](https://miat.inrae.fr/teams/scidyn/)
	- activities of RECORD: implementing models and algorithms, and performing numerical experiments.
	- involved team members: @Ronan.Trépos
-----
* __European Joint Program Soil - Internal Call [CarboSeq](https://ejpsoil.eu/soil-research/carboseq) (2022-2025)__
	- description: The CarboSeq project (Soil organic carbon sequestration potential of agricultural soils in Europe) 
	aims at estimating the soil organic carbon sequestration potential of agricultural soils in Europe.
	- collaborators: [US InfoSol](https://eng-info-et-sols.val-de-loire.hub.inrae.fr/)
	- activities of RECORD: deployment of a simulator (Soil organic carbon sequestration) on SIWAA.
	- involved team members: @Eric.Casellas, @Patrick.Chabrier
-----
* __Exploratory Project [REDELAC](https://www6.inrae.fr/climae/Thematiques/Conception-et-evaluation-de-systemes-durables/REDELAC) 
 of the [CLIMAE](https://www6.inrae.fr/climae/) metaprogramme (2023 - 2024)__
	- description: The REDELAC project (Resilience and Sustainability of Dairy Farms on the plain 
	facing climatic hazards) aims to simulate the medium-term evolution of the functioning of 
	lowland dairy farms, to look at the impact of the future climate and the 
	associated uncertainty. This interdisciplinary exploratory project will be confined 
	to a Brittany territory.
	- collaborators: [UMR PEGASE](https://eng-pegase.rennes.hub.inrae.fr/), [UMR MoSAR](https://www6.jouy.inrae.fr/mosar_eng/UMR-MoSAR)
	- activities of RECORD: data preparation, application deployment, development of a simulation workflow, simulations on HPC
	- involved team members: @Eric.Casellas, @Patrick.Chabrier
-----
	



	 
