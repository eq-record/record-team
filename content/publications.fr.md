---
#title: "Publications"
date: 2025-01-13
draft: false
bib: RECORD
---

* __Pour citer le support du dispositif RECORD à vos travaux__  

    - Vous pouvez mentionner dans vos articles 
    >"Ce travail a été effectué avec le support du dispositif collectif RECORD de l'unité MIAT-INRAE".  
    
    - Vous pouvez également citer l'article de référence :
    >J.-E. Bergez, P. Chabrier, C. Gary, M.H. Jeuffroy, D. Makowski, G. Quesnel, E. Ramat, H. Raynal, N. Rousse, D. Wallach, P. Debaeke, P. Durand, M. Duru, J. Dury, P. Faverdin, C. Gascuel-Odoux, F. Garcia, An open platform to build, evaluate and simulate integrated models of farming and agro-ecosystems, Environmental Modelling & Software, Volume 39, 2013, Pages 39-49, https://doi.org/10.1016/j.envsoft.2012.03.011.

***

* __Liste de publications récentes__

{{< bibliography >}}

