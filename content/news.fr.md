---
#title: "new.fr.md"
date: 2023-11-29
draft: false
---


* __Rencontres Mexico 2024__. Les rencontres du réseau [Mexico](https://reseau-mexico.fr/) 
ont lieu du 5 au 6 Décembre 2024. Le [site](https://mexico2024.sciencesconf.org/) du 
séminaire.

* __Rencontres Mexico 2023__. Les rencontres du réseau [Mexico](https://reseau-mexico.fr/) 
ont lieu du 4 au 5 Décembre 2023. Le [site](https://mexico2023.sciencesconf.org/) du 
séminaire.

* [__XIII Séminaire STICS__](https://stics-bx2023.seminaire.inrae.fr/). le XIII séminaire 
[Stics](https://www6.paca.inrae.fr/stics/) se déroulera du 13 au 16 novembre 2023 à proximité de Bordeaux (Aérocampus Aquitaine - 1, route de Cénac, 33360 Latresne).

* [__Introduction aux méthodes mathématiques et statistiques pour les modèles dynamiques pour l'agriculture__](https://www.acta.asso.fr/formations/methodes-mathematiques-et-statistiques-pour-les-modeles-dynamiques-pour-lagriculture). Une formation organisée par L'[Acta](https://www.acta.asso.fr/) du 19 au 22 juin 2023 à Montpellier qui apporte les bases pour manipuler les principales méthodes statistiques et appréhender la pratique réelle de ces méthodes au travers d'exemples et de travaux
pratiques.

* __Rencontres Mexico 2022__. Les rencontres du réseau [Mexico](https://reseau-mexico.fr/) 
ont lieu du 24 au 25 Novembre 2022. Le [site](https://mexico2022.colloque.inrae.fr/) du 
séminaire.

* __Phenology at the crossroads 2022__. La conférence 
[_Phenology at the crossroads 2022_](https://pheno-2022.colloque.inrae.fr/)  a lieu à Avignon, 
France du 20 au 24 juin.

* __Semaine de la modélisation des cultures 2021__. Une semaine de la modélisation des 
cultures est organisée du 6 au 10 décembre, en présentiel à Montpellier 
([rappel des grandes lignes du programme](files/MFPC_dec2021_rappel.pdf)).

* __Rencontres Mexico 2021__. Les rencontres du réseau [Mexico](https://reseau-mexico.fr/) 
ont lieu du 29 au 30 novembre 2021. Le [site](https://mexico2021.sciencesconf.org/) du 
séminaire.





