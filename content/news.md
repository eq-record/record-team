---
#title: "new.md"
date: 2023-11-29
draft: false
---

* [__Mexico 2024 symposium__](https://mexico2024.sciencesconf.org/). The 2024 
symposium of the network [Mexico](https://reseau-mexico.fr/) 
will take place 6-6 December in Paris, France.

* [__Mexico 2023 symposium__](https://mexico2023.sciencesconf.org/). The 2023 
symposium of the network [Mexico](https://reseau-mexico.fr/) 
will take place 4-5 December in Paris, France.

* [__XIII STICS Seminar__](https://stics-bx2023.seminaire.inrae.fr/). The XIII
[Stics](https://www6.paca.inrae.fr/stics/) Seminar will take place  13-14-15 and 16 November 2023
at the Aérocampus Aquitaine of Floirac.

* [__Introducing statistics methods for dynamic agricultural models__](https://www.acta.asso.fr/formations/methodes-mathematiques-et-statistiques-pour-les-modeles-dynamiques-pour-lagriculture). A training session in French organised by [Acta](https://www.acta.asso.fr/) from June 19 to June 22, 2023 in Montpellier, France.

* [__Mexico 2022 symposium__](https://mexico2022.colloque.inrae.fr/). The 2022 
symposium of the network [Mexico](https://reseau-mexico.fr/) 
will take place 24-25 November in Bordeaux, France.

* __Phenology at the crossroads 2022__. The conference 
[_Phenology at the crossroads 2022_](https://pheno-2022.colloque.inrae.fr/) 
will take place in 20-24 June in Avignon, France.

* __Week of Crop modeling 2021__. One week dedicated to the crop modeling will take place 
6-10 Decmber in Montpellier. [Here](files/MFPC_dec2021_rappel.pdf) is a quick overview of the content.

* [__Mexico 2021 symposium__](https://mexico2021.sciencesconf.org/). The 2021 
symposium of the network [Mexico](https://reseau-mexico.fr/) 
will take place 29-30 November in Toulouse, France.






