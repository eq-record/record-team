Howto
=====

## Render on your personnal computer the site?

* Install Hugo, https://gohugo.io/getting-started/installing
* Clone this project : `git clone --recurse-submodules git@forgemia.inra.fr:eq-record/record-team.git`
* Go inside the root folder
* Launch the site locally, and you should get some thing like this:

```
$ cd record-team/
$ hugo server --disableFastRender
Start building sites … 

                   | EN  
-------------------+-----
  Pages            |  7  
  Paginator pages  |  0  
  Non-page files   |  0  
  Static files     | 11  
  Processed images |  0  
  Aliases          |  0  
  Sitemaps         |  1  
  Cleaned          |  0  

Built in 21 ms
Watching for changes in /home/pchabrier/DEVS/record-team/{archetypes,content,data,layouts,static,themes}
Watching for config changes in /home/pchabrier/DEVS/record-team/config.toml
Environment: "development"
Serving pages from memory
Web Server is available at http://localhost:1313/record-team/ (bind address 127.0.0.1)
Press Ctrl+C to stop
```

## Update the site?

* push to this git project
* a pipeline is is in place to render the new site (CI/CD)
* have a look here https://eq-record.pages.mia.inra.fr/record-team/

